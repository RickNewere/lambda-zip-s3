terraform {
  backend "s3" {}
}

provider "aws" {
  region = "us-east-2" 
}

variable "CI_COMMIT_SHORT_SHA" {
  type = string
}

variable "aws_account_id" {
  type = string
}


data "archive_file" "lambda_zip" {
  type        = "zip"
  source_file = "src/main.py"
  output_path = "${var.CI_COMMIT_SHORT_SHA}.zip"
}

resource "aws_lambda_function" "lambda" {
  filename      = data.archive_file.lambda_zip.output_path
  function_name = "lambda_function_name"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "main.handler"
  source_code_hash = data.archive_file.lambda_zip.output_base64sha256
  runtime = "python3.8"
}

resource "aws_iam_role" "iam_for_lambda" {
  name                 = "GAPMGM-iam_for_lambda"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

output "lambda_function_arn" {
  value = aws_lambda_function.lambda.arn
}